# Base image with Java application server (e.g., Tomcat)
FROM tomcat:10.1-jdk17-corretto

# Set the working directory inside the container
WORKDIR /usr/local/tomcat/webapps/

# Copy the WAR file from the target folder to the Tomcat webapps directory
COPY target/*.war application.war

# Expose the Tomcat default port
EXPOSE 8080

# Start Tomcat when the container runs
CMD ["catalina.sh", "run"]